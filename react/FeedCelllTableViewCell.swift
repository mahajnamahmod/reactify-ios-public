//
//  FeedCelllTableViewCell.swift
//  react
//
//  Created by Mahmod Mahajna on 9/9/17.
//  Copyright © 2017 bilaltayh. All rights reserved.
//

import UIKit

class FeedCelllTableViewCell: UITableViewCell {
    @IBOutlet weak var friendImg: UIImageView!
    @IBOutlet weak var friendLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
