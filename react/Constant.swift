//
//  Constant.swift
//  ButtonUp
//
//  Created by Eyal Silberman on 18/05/2017.
//  Copyright © 2017 200apps. All rights reserved.
//

import Foundation

let DEV_DOMAIN = "https://84ynbzexbj.execute-api.us-east-1.amazonaws.com/dev/"


let LOGIN_PATH = "/login"
let SIGNUP_PATH = "/createUser"
let GET_FRIENDS = "/getFriends"
struct Body {
    var username: String
    var url: String
    
}



let POST_VIDEO = "video"
let GET_VIDEO_UPLOAD_URL = "getVideoUploadUrl"



let NEXT_VIDEO = "next_video"
