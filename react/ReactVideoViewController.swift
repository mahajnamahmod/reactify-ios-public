
import UIKit
import AVFoundation
import AVKit
//import Cloudinary
import SwiftyJSON
import Alamofire
class ReactVideoViewController: UIViewController {
    var rel: UILabel!
    
    var approve = UIButton()
    var cancelButton = UIButton()
    @IBOutlet weak var approveFrame: UIView!
    @IBOutlet weak var cancelFrame: UIView!
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private var videoURL: URL
    var player: AVPlayer?
    var playerController : AVPlayerViewController?
    
    init(videoURL: URL) {
        self.videoURL = videoURL
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.gray
        player = AVPlayer(url: videoURL)
        playerController = AVPlayerViewController()
        
        guard player != nil && playerController != nil else {
            return
        }
        playerController!.showsPlaybackControls = false
        
        playerController!.player = player!
        self.addChildViewController(playerController!)
        self.view.addSubview(playerController!.view)
        playerController!.view.frame = view.frame
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player!.currentItem)
         cancelButton = UIButton(frame: CGRect(x: 16, y: 567, width: 100, height: 100))
        cancelButton.setImage(#imageLiteral(resourceName: "cancel"), for: UIControlState())
        cancelButton.imageView?.contentMode = .scaleAspectFill
        
        
        cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        view.addSubview(cancelButton)
         approve = UIButton(frame: CGRect(x: 259, y: 567, width: 100, height: 100))
        approve.setImage(#imageLiteral(resourceName: "+_icon"), for: UIControlState())
        approve.imageView?.contentMode = .scaleAspectFill
        approve.addTarget(self, action: #selector(approveFunc), for: .touchUpInside)
        view.addSubview(approve)
        
        rel = UILabel(frame: CGRect(x: 150, y: 567, width: 200, height: 100))
        rel.text = "Uploading...."
        rel.textColor = UIColor.white
        rel.font = rel.font.withSize(20)
        rel.isHidden = true
        view.addSubview(rel)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        player?.play()
    }
    
    func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    func watch(){
        
    }
    @objc fileprivate func playerItemDidReachEnd(_ notification: Notification) {
        if self.player != nil {
            self.player!.seek(to: kCMTimeZero)
            self.player!.play()
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    func approveFunc(){
        Back = 1
        Tt = 1
        cancelButton.isHidden = true
        approve.isHidden = true
        rel.isHidden = false
        var data: Data?
        
        print(self.videoURL)
        do{
            data = try NSData(contentsOfFile: (self.videoURL.relativePath), options: NSData.ReadingOptions.alwaysMapped) as Data
        } catch {
            print("hasdhasdhas")
            return
        }
        let headers: [String: Any?] = [
            "username" : userName,
            "watchedVid" : vidUrl,
            "friendVidOwner": from
            ]
        API.shared.getReactionUploadUrl(headers: headers, completionHandler: {(success, data) in
            print("======DATA=======")
            let json = JSON(data)
            
            if let url = json["url"].string{
                print("Trying to upload to url: \(url)")
                self.up(url: url, thumburl: json["thumbUrl"].string!)
            }
        })
        Timer.scheduledTimer(timeInterval: TimeInterval(3), target: self, selector: #selector(watch), userInfo: nil, repeats: false)
        
    }
    func up(url: String, thumburl: String) {
        

        do {
            let pathArray = self.videoURL.path.components(separatedBy: "/")
            let fileNameArray = pathArray[pathArray.count - 1].components(separatedBy: ".")
            let fileName = fileNameArray[0]
            
            let videodata = try? Data(contentsOf: self.videoURL)
            
            let asset = AVURLAsset(url: self.videoURL , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            let imageData = UIImageJPEGRepresentation(thumbnail, 0.1)
            let base64String = imageData!.base64EncodedString(options: .lineLength64Characters)
            
            Alamofire.upload(videodata!, to: url, method: .put).responseString(completionHandler: { response in
                if(response.result.isSuccess){
                    Alamofire.upload(imageData!, to: thumburl, method: .put).responseString(completionHandler: { response2 in
                        if(response2.result.isSuccess){
                            self.player!.pause()
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            let window = appDelegate.window
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let rootController = mainStoryboard.instantiateViewController(withIdentifier: "scrollViewMain") as! scrollViewController
                            UIView.transition(with: window!, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {() -> Void in window?.rootViewController = rootController}, completion: nil)
                        }
                        
                    })
                }
            })
            
        } catch let error {
            print("* Error generating thumbnail: \(error.localizedDescription)")
        }
        
    }
    
    
    
    
    
}

//import UIKit
//import AVFoundation
//import AVKit
////import Cloudinary
//import SwiftyJSON
//import Alamofire
//class ReactVideoViewController: UIViewController {
//    var rel: UILabel!
//    
//    var approve = UIButton()
//    var cancelButton = UIButton()
//    @IBOutlet weak var approveFrame: UIView!
//    @IBOutlet weak var cancelFrame: UIView!
//    override var prefersStatusBarHidden: Bool {
//        return true
//    }
//    
//    private var videoURL: URL
//    var player: AVPlayer?
//    var playerController : AVPlayerViewController?
//    
//    init(videoURL: URL) {
//        self.videoURL = videoURL
//        super.init(nibName: nil, bundle: nil)
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    override func viewDidLayoutSubviews() {
//        
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.view.backgroundColor = UIColor.gray
//        player = AVPlayer(url: videoURL)
//        playerController = AVPlayerViewController()
//        
//        guard player != nil && playerController != nil else {
//            return
//        }
//        playerController!.showsPlaybackControls = false
//        
//        playerController!.player = player!
//        self.addChildViewController(playerController!)
//        self.view.addSubview(playerController!.view)
//        playerController!.view.frame = view.frame
//        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player!.currentItem)
//        cancelButton = UIButton(frame: CGRect(x: 16, y: 567, width: 100, height: 100))
//        cancelButton.setImage(#imageLiteral(resourceName: "cancel"), for: UIControlState())
//        cancelButton.imageView?.contentMode = .scaleAspectFill
//        
//        
//        cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
//        view.addSubview(cancelButton)
//        approve = UIButton(frame: CGRect(x: 259, y: 567, width: 100, height: 100))
//        approve.setImage(#imageLiteral(resourceName: "+_icon"), for: UIControlState())
//        approve.imageView?.contentMode = .scaleAspectFill
//        approve.addTarget(self, action: #selector(approveFunc), for: .touchUpInside)
//        view.addSubview(approve)
//        
//        rel = UILabel(frame: CGRect(x: 150, y: 567, width: 200, height: 100))
//        rel.text = "loading...."
//        rel.textColor = UIColor.purple
//        rel.font = rel.font.withSize(30)
//        rel.isHidden = true
//        view.addSubview(rel)
//    }
//    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        player?.play()
//    }
//    
//    func cancel() {
//        dismiss(animated: true, completion: nil)
//    }
//    
//    func watch(){
//        
//    }
//    @objc fileprivate func playerItemDidReachEnd(_ notification: Notification) {
//        if self.player != nil {
//            self.player!.seek(to: kCMTimeZero)
//            self.player!.play()
//        }
//    }
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    func approveFunc(){
//        cancelButton.isHidden = true
//        approve.isHidden = true
//        rel.isHidden = false
//        
//        var data: Data?
//        
//        print(self.videoURL)
//        do{
//            data = try NSData(contentsOfFile: (self.videoURL.relativePath), options: NSData.ReadingOptions.alwaysMapped) as Data
//        } catch {
//            print("hasdhasdhas")
//            return
//        }
//        let headers: [String: Any?] = [
//            "username" : userName,
//            "watchedVid" : vidUrl,
//            "friendVidOwner": from
//        ]
//        API.shared.getReactionUploadUrl(headers: headers, completionHandler: {(success, data) in
//            print("======DATA=======")
//            let json = JSON(data)
//            
//            if let url = json["url"].string{
//                print("Trying to upload to url: \(url)")
//                self.up(url: url, thumburl: json["thumbUrl"].string!)
//            }
//        })
//        Timer.scheduledTimer(timeInterval: TimeInterval(3), target: self, selector: #selector(watch), userInfo: nil, repeats: false)
//        
//        
//    }
//    func up(url: String, thumburl: String) {
//        
//        
//        do {
//            let pathArray = self.videoURL.path.components(separatedBy: "/")
//            let fileNameArray = pathArray[pathArray.count - 1].components(separatedBy: ".")
//            let fileName = fileNameArray[0]
//            
//            let videodata = try? Data(contentsOf: self.videoURL)
//            
//            let asset = AVURLAsset(url: self.videoURL , options: nil)
//            let imgGenerator = AVAssetImageGenerator(asset: asset)
//            imgGenerator.appliesPreferredTrackTransform = true
//            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
//            let thumbnail = UIImage(cgImage: cgImage)
//            let imageData = UIImageJPEGRepresentation(thumbnail, 0.1)
//            let base64String = imageData!.base64EncodedString(options: .lineLength64Characters)
//            
//            Alamofire.upload(videodata!, to: url, method: .put).responseString(completionHandler: { response in
//                if(response.result.isSuccess){
//                    Alamofire.upload(imageData!, to: thumburl, method: .put).responseString(completionHandler: { response2 in
//                        if(response2.result.isSuccess){
//                            self.dismiss(animated: true, completion: nil)
//                        }
//                        
//                    })
//                }
//            })
//            
//        } catch let error {
//            print("* Error generating thumbnail: \(error.localizedDescription)")
//        }
//        
//    }
//    
//    
//    
//    
//    
//}











