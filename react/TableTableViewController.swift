//
//  TableTableViewController.swift
//  react
//
//  Created by Mahmod Mahajna on 9/9/17.
//  Copyright © 2017 bilaltayh. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage


var Tt = 0

class TableTableViewController: UITableViewController {
    struct cellData{
        let cell: Int!
        let text: String!
        let user: String!
        var imageUrl: String!
        let link : String!
    }
    var myScrollView = UIScrollView()
    @IBOutlet var myTableView: UITableView!
    var cache:NSCache<AnyObject, AnyObject>!

    var myArrayOfCellData = [cellData]()
    
    
    override func viewDidAppear(_ animated: Bool) {
        print("hiiiiiii")
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.myTableView.separatorStyle = .none
        myArrayOfCellData = [cellData(cell: 2, text: "", user:"", imageUrl: "",link:"")]
        myTableView.bounces = false
        self.cache = NSCache()
        let parameters: [String: Any?] = [
            "username" : userName
        ]
        if(Tt != 1){
            Tt = 0
            reload()
            
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return myArrayOfCellData.count
        
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if(myArrayOfCellData[indexPath.row].cell == 1 || myArrayOfCellData[indexPath.row].cell == 6){
            
            
            var image = #imageLiteral(resourceName: "default").resized(to: CGSize(width: 60, height: 60))
            
                
            
            if(self.myArrayOfCellData[indexPath.row].imageUrl != ""){
                if (self.cache.object(forKey: (self.myArrayOfCellData[indexPath.row].imageUrl as NSString) as AnyObject) == nil){
                    let url = URL(string: (self.myArrayOfCellData[indexPath.row]as cellData).imageUrl)
                    let urlData = try? Data(contentsOf: url!)
                    if let imageData = urlData{
                        image = UIImage(data: imageData)!.resized(to: CGSize(width: 60, height: 60))
                    }
                     self.cache.setObject(image, forKey: ((self.myArrayOfCellData[indexPath.row]as cellData).imageUrl as NSString) as AnyObject)
                }else{
                    image = self.cache.object(forKey: (self.myArrayOfCellData[indexPath.row]as cellData).imageUrl as NSString) as! UIImage

                }
            
            }
            
            let cell = Bundle.main.loadNibNamed("FeedCelllTableViewCell", owner: self, options: nil)?.first as!FeedCelllTableViewCell
            cell.friendLabel?.text = (myArrayOfCellData[indexPath.row] as cellData).text
            cell.friendImg?.image = image
            cell.friendImg?.layer.borderWidth = 2
            cell.friendImg?.layer.borderColor = UIColor(red:0.64, green:0.61, blue:0.91, alpha:1.0).cgColor
            cell.friendImg?.layer.cornerRadius = (cell.friendImg?.frame.height)!/2
            cell.friendImg?.contentMode = .scaleAspectFill
            cell.friendImg?.clipsToBounds = true
            return cell
        }
        else if(myArrayOfCellData[indexPath.row].cell == 2){
            let cell = Bundle.main.loadNibNamed("TableViewCell", owner: self, options: nil)?.first as!TableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        else if(myArrayOfCellData[indexPath.row].cell == 3){
            let cell = Bundle.main.loadNibNamed("separatorFriendTableViewCell", owner: self, options: nil)?.first as!separatorFriendTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        else if(myArrayOfCellData[indexPath.row].cell == 4){
            let cell = Bundle.main.loadNibNamed("reactionsSeparatorTableViewCell", owner: self, options: nil)?.first as!reactionsSeparatorTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        else if(myArrayOfCellData[indexPath.row].cell == 9 ){
            let cell = Bundle.main.loadNibNamed("NoReactViewCell", owner: self, options: nil)?.first as!NoReactViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        else if( myArrayOfCellData[indexPath.row].cell == 8){
            let cell = Bundle.main.loadNibNamed("NoFriendViewCell", owner: self, options: nil)?.first as!NoFriendViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        else{
            let cell = Bundle.main.loadNibNamed("blankTableViewCell", owner: self, options: nil)?.first as!blankTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        
        
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(myArrayOfCellData[indexPath.row].cell == 1 || myArrayOfCellData[indexPath.row].cell == 6){
            return 60
        }
        else if(myArrayOfCellData[indexPath.row].cell == 2){
            return 68
        }
        else if(myArrayOfCellData[indexPath.row].cell == 3){
           return 24
        }
        else if(myArrayOfCellData[indexPath.row].cell == 4){
            return 24
        }
        else if(myArrayOfCellData[indexPath.row].cell == 9 || myArrayOfCellData[indexPath.row].cell == 8){
            return 77
        }
        else{
            return 9
        }
    }
    
    func clearOnAppearance() {
        for indexPath in tableView.indexPathsForSelectedRows ?? [] {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(myArrayOfCellData[indexPath.row].cell == 1){
            vidUrl = myArrayOfCellData[indexPath.row].link
            from = myArrayOfCellData[indexPath.row].user
            self.performSegue(withIdentifier: "watch1Segue", sender: self)
        }
        if(myArrayOfCellData[indexPath.row].cell == 6){
            vidUrl = myArrayOfCellData[indexPath.row].link
            from = myArrayOfCellData[indexPath.row].user
            self.performSegue(withIdentifier: "watch2Segue", sender: self)
        }
       }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func reload(){
       //  DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            var friends = [String]()
            
            
            self.myArrayOfCellData = [cellData(cell: 2, text: "", user:"", imageUrl: "",link:""),cellData(cell: 3, text: "", user:"", imageUrl: "",link:"")]
            let parameters2: [String: Any?] = [
                "username": userName,
                "friends": ["mahmod", "zeema"]
            ]
            
            API.shared.getAllFriendsVideos(parameters: parameters2, retriesIntervals: [1,1,1,3,3,3,9,9,9], completionHandler: {(success, data) in
                if(success){
                    let json2 = JSON(data)
                    print("i got ============================================================================")
                    print(json2)
                    var t = 0
                    for  j in json2["Items"]{
                        
                        var image = j.1["Items"][0]["thumbUrl"].string!
                        let l = j.1["Items"][0]["link"].string!
                        let friend = cellData(cell : 1, text : "                     "+j.1["Items"][0]["username"].string!,user:j.1["Items"][0]["username"].string!, imageUrl:image,link:l)
                        if(t != 0){
                            self.myArrayOfCellData.append(cellData(cell: 0, text: "",user:"", imageUrl: "", link:""))
                        }
                        t = t + 1
                        self.myArrayOfCellData.append(friend)
                    }
                    
                    if(t == 0){
                        self.myArrayOfCellData.append(cellData(cell: 8, text: "", user:"", imageUrl: "",link:""))
                    }
                    
                    self.myArrayOfCellData.append(cellData(cell: 4, text: "", user:"", imageUrl: "",link:""))
                    
                    var c = 0
                    for  p in json2["reactions"]{
                        

                        let l2 = p.1["link"].string!
                        let friend2 = cellData(cell : 6, text : "                     "+p.1["sender"].string!,user:p.1["sender"].string!, imageUrl:p.1["thumbUrl"].string!,link:l2)
                        if(c != 0){
                        self.myArrayOfCellData.append(cellData(cell: 0, text: "",user:"", imageUrl: "", link:""))
                      }
                        c = c + 1
                        self.myArrayOfCellData.append(friend2)
                }
                if(c == 0){
                    self.myArrayOfCellData.append(cellData(cell: 9, text: "", user:"", imageUrl: "",link:""))
                }
            }
                
           self.tableView.reloadData() })

        }
        

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
  //  }
}













