

import UIKit
import MediaPlayer
import SwiftyJSON


var vidUrl = String()
var from = String()
class WatchViewController: UIViewController, UIGestureRecognizerDelegate {
    override var prefersStatusBarHidden: Bool {
        return true
    }
    @IBOutlet weak var videoFrame: UIView!
    var player : MPMoviePlayerController!
   // let url = URL(string: "https://res.cloudinary.com/glamhuji/video/upload/c_scale,h_1920,q_50/v1495144861/IMG_1073_ln0owk.mov")

    override func viewDidLoad() {
        super.viewDidLoad()
        playVideo(string: vidUrl)
        let tap =  UITapGestureRecognizer(target: self, action: #selector(finish))

    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    func playVideo(string: String){
        let url1 = URL(string: string)

        player = MPMoviePlayerController(contentURL: url1)
        player.repeatMode = .one
        player.view.frame = videoFrame.frame
        self.view.addSubview(player.view)
        player.controlStyle = .none
        player.isFullscreen = true
        self.player.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(finish)))
        
        let viewForCancel = UIView(frame: self.view.frame)
        viewForCancel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(finish)))
        viewForCancel.backgroundColor = UIColor.clear
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(finish)))
        self.view.addSubview(viewForCancel)

    }
    
    func finish(){
        player.pause()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let window = appDelegate.window
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = mainStoryboard.instantiateViewController(withIdentifier: "reactifyRecord") as! ReactViewController
        UIView.transition(with: window!, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {() -> Void in window?.rootViewController = rootController}, completion: nil)
    }
    
    @IBAction func UnfoldCircleTapped(_ sender: Any) {
        player.pause()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let window = appDelegate.window
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = mainStoryboard.instantiateViewController(withIdentifier: "react") as! ReactViewController
        UIView.transition(with: window!, duration: 0.2, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {() -> Void in window?.rootViewController = rootController}, completion: nil)
    }

 

}
