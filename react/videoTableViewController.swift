//
//  videoTableView.swift
//  react
//
//  Created by bilaltayh on 9/11/17.
//  Copyright © 2017 bilaltayh. All rights reserved.
//

import UIKit

var friendName = ""

class videoTableViewController: UITableViewController {

    var video : [String?] = []


    @IBOutlet weak var myTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.video = ["","bilaltayh    21/07/2018  12:00:00","mahmodmahajna    20/07/2018  03:09:00","mahmod","taya","ma7ajna","bilal","10/5","dsg"," 234"," sdfsdf sdf sdf sdf sd ","dasfdsdaf dsf sdf asdf"]
        self.myTable.tableFooterView = UIView()
        self.myTable.bounces = false
        self.automaticallyAdjustsScrollViewInsets = false
        self.myTable.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
        self.myTable.layer.backgroundColor = UIColor.lightGray.cgColor
        self.tableView.contentOffset = CGPoint(x: 0,y:-65)
        definesPresentationContext = true
        myTable.delegate = self
    }
    

    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.video.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row as Int == 0){
            return 0
        }
        return 200
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.video[indexPath.row]
        cell.textLabel?.font = UIFont(name: (cell.textLabel?.font.familyName)!, size:30)
        return cell
    }
    
    func clearOnAppearance() {
        for indexPath in tableView.indexPathsForSelectedRows ?? [] {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //play the video
        clearOnAppearance()
    }

}
