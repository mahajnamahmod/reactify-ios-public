//
//  searchTableViewController.swift
//  react
//
//  Created by bilaltayh on 9/9/17.
//  Copyright © 2017 bilaltayh. All rights reserved.
//

import UIKit
import SwiftyJSON


protocol friendDelegate {
    func reload()
}


class searchTableViewController: UITableViewController , UISearchResultsUpdating{
    
    @IBOutlet var myTableViewSearch: UITableView!
    var friendsTable = [String]()
    var friendsImage = [UIImage]()
    var searchTable = [String]()
    var searchImage = [UIImage]()
    var searchController : UISearchController!
    var resultController = UITableViewController()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reload()
        
        self.searchTable = []
        self.searchImage = []
        self.myTableViewSearch.tableFooterView = UIView()
        self.myTableViewSearch.bounces = false
        self.automaticallyAdjustsScrollViewInsets = false
        self.resultController.tableView.dataSource = self
        self.resultController.tableView.delegate = self
        self.myTableViewSearch.contentInset = UIEdgeInsetsMake(63, 0, 0, 0)
        self.myTableViewSearch.layer.backgroundColor = UIColor.white.cgColor

        self.searchController = UISearchController(searchResultsController : self.resultController)
        self.searchController.hidesNavigationBarDuringPresentation = true
        self.searchController.searchBar.barTintColor = UIColor(red:0.69, green:0.66, blue:0.91, alpha:1.0)
        self.tableView.tableHeaderView = self.searchController.searchBar
        //  self.tableView.contentOffset = CGPoint(x: 0,y:-65)
        self.searchController.searchResultsUpdater = self
        definesPresentationContext = true
        myTableViewSearch.delegate = self
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        let parameters: [String: Any?] = [
            "userQuery" : searchController.searchBar.text!,
            "callerName": userName
        ]
        API.shared.searchName(parameters: parameters, retriesIntervals: [1,1,1,3,3,3,9,9,9], completionHandler: {(success, data) in
            if(success){
                let json = JSON(data)
                self.searchTable = []
                self.searchImage = []
                    for  i in json["Items"]{
                        let url = URL(string: i.1["img"].string!)
                        let urlData = try? Data(contentsOf: url!)
                        var image = UIImage()
                        if let imageData = urlData{
                            image = UIImage(data: imageData)!.resized(to: CGSize(width: 150, height: 150))
                        }else{
                            image = #imageLiteral(resourceName: "default").resized(to: CGSize(width: 150, height: 150))
                        }
                        self.searchImage.append(image)
                        self.searchTable.append(i.1["username"].string!)
                    }
                    self.tableView.reloadData()
                    self.resultController.tableView.reloadData()
            }
            
        })
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.tableView){
            return self.friendsTable.count
        }
        else{
            return self.searchTable.count
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == self.tableView){
            
            let cell = UITableViewCell()
            cell.textLabel?.text = self.friendsTable[indexPath.row]
            return cell
        }
        else{
            let cell = UITableViewCell()
            cell.textLabel?.text = self.searchTable[indexPath.row]
            return cell
        }
    }
    
    func clearOnAppearance() {
        for indexPath in tableView.indexPathsForSelectedRows ?? [] {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView != self.tableView){
            let a = self.searchTable[indexPath.row] as String!
            let b = self.searchImage[indexPath.row] as UIImage!
            let popup = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addPopup") as! popupAddFriendViewController
            popup.passedName = a!
            popup.passedImg = b!
            popup.passedSearch = self.myTableViewSearch
            self.searchController.dismiss(animated: false, completion: nil)
            //            self.searchController.isActive = false
            popup.tableView = self.tableView
            self.addChildViewController(popup)
            popup.view.frame = self.view.frame
            self.view.addSubview(popup.view)
            popup.delegate = self
            
            popup.didMove(toParentViewController: self)
            clearOnAppearance()
        }
        else{
            let popup = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "deletePopup") as! popupDeleteFriendViewController
            popup.passedName = self.friendsTable[indexPath.row]
            popup.passedSearch = self.myTableViewSearch
            popup.passedImg = self.friendsImage[indexPath.row]
            popup.tableView = self.tableView
            self.addChildViewController(popup)
            popup.view.frame = self.view.frame
            self.view.addSubview(popup.view)
            popup.delegate = self
            popup.didMove(toParentViewController: self)
            clearOnAppearance()
        }
        
    }
    
    
    
}

extension searchTableViewController: friendDelegate {
    func reload(){
        
         self.friendsTable = []
         self.friendsImage = []
        let parameters: [String: Any?] = [
            "username" : userName
        ]
        
        API.shared.getFriends(parameters: parameters, retriesIntervals: [1,1,1,3,3,3,9,9,9], completionHandler: {(success, data) in
            if(success){
                let json = JSON(data)
                if(json["statusCode"] == 200){
                    print("????????????????????????????????????????")
                    print(json)
                    let parameters: [String: String?]!
                    
                    for  i in json["data"]{
                        print("the ussseeeeeeeeeers")
                        print(i)
                        let url = URL(string: i.1["img"].string!)
                        let urlData = try? Data(contentsOf: url!)
                        print(urlData)
                        var image = UIImage()
                        if let imageData = urlData{
                            image = UIImage(data: imageData)!.resized(to: CGSize(width: 150, height: 150))
                        }else{
                            image = #imageLiteral(resourceName: "default").resized(to: CGSize(width: 150, height: 150))
                        }
                         self.friendsImage.append(image)
                         self.friendsTable.append(i.1["username"].string!)
                    self.tableView.reloadData()
                    
                    }
                }
            }
        })
    }
}
