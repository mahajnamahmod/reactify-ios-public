//
//  API.swift
//  ButtonUp
//
//  Created by Eyal Silberman on 18/05/2017.
//  Copyright © 2017 200apps. All rights reserved.
//

import Foundation
import Alamofire

class API {
    
    static let shared = API()
    var method: HTTPMethod = .get
    var headers: HTTPHeaders = [:]
    
    
    func login(parameters: [String: Any?], retriesIntervals: [TimeInterval], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: LOGIN_PATH, method: .post, parameters: parameters, headers: [:], retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: completionHandler)

        
    }
    
    func addFriend(parameters: [String: Any?], retriesIntervals: [TimeInterval], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: "/addFriend", method: .post, parameters: parameters, headers: [:], retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: completionHandler)
        
        
    }
    
    
    
    func getReactions(parameters: [String: Any?], retriesIntervals: [TimeInterval], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: "/getMyReactions", method: .post, parameters: parameters, headers: [:], retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: completionHandler)
        
        
    }
    
    
    func getReactionUploadUrl(headers: [String: Any?], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: "/getReactionsUploadUrl", method: .post, parameters: [:], headers: headers as! HTTPHeaders, retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: {(success, data) in
            if success {
                completionHandler!(success,data)
            }
        })
    }
    
    func deleteFriends(parameters: [String: Any?], retriesIntervals: [TimeInterval], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: "/deleteFriends", method: .post, parameters: parameters, headers: [:], retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: completionHandler)
        
        
    }
    
    func getAllFriendsVideos(parameters: [String: Any?], retriesIntervals: [TimeInterval], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: "/getAllFriendsVideos", method: .post, parameters: parameters, headers: [:], retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: completionHandler)
        
        
    }
    
    
    
    
    
    
    func searchName(parameters: [String: Any?], retriesIntervals: [TimeInterval], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: "/searchName", method: .post, parameters: parameters, headers: [:], retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: completionHandler)
        
        
    }
    
    
    
    func getFriends(parameters: [String: Any?], retriesIntervals: [TimeInterval], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: GET_FRIENDS, method: .post, parameters: parameters, headers: [:], retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: completionHandler)
        
        
    }

    func uploadUrl(headers: [String: Any?], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: GET_VIDEO_UPLOAD_URL, method: .post, parameters: [:], headers: headers as! HTTPHeaders, retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: {(success, data) in
            if success {
                completionHandler!(success,data)
            }
        })
    }
    
    func uploadImg(headers: [String: Any?], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: "/uploadImg", method: .post, parameters: [:], headers: headers as! HTTPHeaders, retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: {(success, data) in
            if success {
                completionHandler!(success,data)
            }
        })
    }
    
    func createUser(parameters: [String: Any?], retriesIntervals: [TimeInterval], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: SIGNUP_PATH, method: .post, parameters: parameters, headers: [:], retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: completionHandler)
        
        
    }
    
    func removeReactionFromUser(parameters: [String: Any?], retriesIntervals: [TimeInterval], completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: "/removeReactionFromUser", method: .post, parameters: parameters, headers: [:], retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: completionHandler)
        
        
    }
    
    func getVid(completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?)
    {
        _ = PostRequest(host: DEV_DOMAIN, path: NEXT_VIDEO, method: .post, parameters: ["username": "x"], headers: [:], retriesIntervals: [1,1,1,3,3,3,9,9,9], priority: .high, completionHandler: {(success, data) in
            if success {
                completionHandler!(success,data)
            }
        })
    }
    
}
