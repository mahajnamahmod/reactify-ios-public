//
//  createAccountViewController.swift
//  react
//
//  Created by bilaltayh on 9/7/17.
//  Copyright © 2017 bilaltayh. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

extension UIImage {
    func resized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
}


class createAccountViewController: UIViewController, UITextFieldDelegate,  UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    override func shouldPerformSegue(withIdentifier identifier: String,sender: Any?) -> Bool{
        if(identifier == "showLogin"){
            return true
        }
        return false
    }
    
    @IBOutlet weak var displayImage: UIImageView!
    let myPickerController = UIImagePickerController()
    @IBOutlet weak var invalidCreatMsg: UILabel!
    @IBOutlet weak var emailF: UITextField!
    @IBOutlet weak var usernameF: UITextField!
    @IBOutlet weak var passwordF: UITextField!
    var orginalImg = UIImageView()
    
    
    func isValidEmail(testStr:String) -> Bool {
         print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
//    
//    let headers: [String: Any?] = [
//        "username": username,
//        "password": password,
//        "email": email
//    ]
//    API.shared.uploadImg(headers:headers ,completionHandler: {(success, data) in
//    print("======DATA=======")
//    let json = JSON(data)
//    
//    
//    if let url = json["url"].string{
//    print("Trying to upload to url: \(url)")
//    up(url: url, imgUrl: self.displayImage.image!)
//    }
//    
    
    
    
    
    @IBAction func createAccount(_ sender: Any) {
        let email = emailF.text
        let username = usernameF.text
        let password = passwordF.text
        // check email
        invalidCreatMsg.isHidden = true
        if(isValidEmail(testStr: email!)){
            if((username?.characters.count)! >= 6){
                if((password?.characters.count)! >= 6){
                    let headers: [String: Any?] = [
                        "email" : email,
                        "username" : username,
                        "password" : password
                    ]
                    API.shared.uploadImg(headers:headers , completionHandler: {(success, data) in
                        if(success){
                            print("00000000000000000000000")
                            print(data)
                            let json = JSON(data)
                            if let url = json["url"].string{
                                print(json["url"])
                                self.up(url: json["url"].string!,imgUrl: self.orginalImg.image!)
                                userName = username!
                                self.performSegue(withIdentifier: "imageSegue", sender: self)
                            }
                            else{
                                print(data)
                                self.invalidCreatMsg.text = "check your internet connection"
                                self.invalidCreatMsg.isHidden = false
                                print(json["usernameExists"])
                                if(json["usernameExists"] == "true"){
                                     self.invalidCreatMsg.text = "username already exist"
                                    self.invalidCreatMsg.isHidden = false
                                }
                                if(json["emailExists"] == "true"){
                                    self.invalidCreatMsg.text = "email already exist"
                                    self.invalidCreatMsg.isHidden = false
                                }
                            }
                        }else{
                            self.invalidCreatMsg.text = "check your internet connection"
                            self.invalidCreatMsg.isHidden = false
                            
                        }
                    })                }
                else{
                    invalidCreatMsg.isHidden = false
                    invalidCreatMsg.text = "password characters should be at least 6"
                }
            }
            else{
                invalidCreatMsg.isHidden = false
                invalidCreatMsg.text = "username characters should be at least 6"
            }
        }
        else{
            invalidCreatMsg.isHidden = false
            invalidCreatMsg.text = "email is invalid"
        }
        
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    
    
    
    
    
    
    func tappedMe()
    {
        myPickerController.delegate = self;
        myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(myPickerController, animated: true, completion: nil)
    }
    
    
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print("here111")
        if let image = info[UIImagePickerControllerOriginalImage]  as? UIImage  {
            print(image)
            orginalImg.image = image
            displayImage.image = image.resized(to: CGSize(width: 110, height: 110))
        }
        picker.dismiss(animated: true, completion: nil);
        print("here555")
    }
    
    
    
    
    
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        print("here111")
//        
//        
//        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
//        let imageUrl          = info[UIImagePickerControllerReferenceURL] as? NSURL
//        let imageName         = imageUrl?.lastPathComponent
//        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
//        let photoURL          = NSURL(fileURLWithPath: documentDirectory)
//        let headers: [String: Any?] = [
//            "username": "mahmodsssss",
//            "password": "mahmodsssss",
//            "email": "jasdkajsdasd"
//        ]
//        let localPath         = photoURL.appendingPathComponent(imageName!)
//        API.shared.uploadImg(headers:headers ,completionHandler: {(success, data) in
//            print("======DATA=======")
//            let json = JSON(data)
//
//            
//            if let url = json["url"].string{
//                print("Trying to upload to url: \(url)")
//                self.up(url: url, imgUrl: image)
//            }
//        })
//        
//    }
    
    func up(url: String, imgUrl: UIImage) {
        print(imgUrl)
        let imageData = UIImageJPEGRepresentation(imgUrl, 0.1)
        let base64String = imageData!.base64EncodedString(options: .lineLength64Characters)
        
        Alamofire.upload(imageData!, to: url, method: .put).responseString(completionHandler: { response in
            print("AFTER UPLOAD VIDEO")
            print(response)
            debugPrint(response)
        })
    }

    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(createAccountViewController.tappedMe))
        displayImage.addGestureRecognizer(tap)
        displayImage.isUserInteractionEnabled = true
        displayImage.image = #imageLiteral(resourceName: "default").resized(to: CGSize(width: 150, height: 150))
        // Do any additional setup after loading the view.
        passwordF.isSecureTextEntry = true
        emailF.placeholder = "Email"
        passwordF.placeholder = "Password"
        usernameF.placeholder = "Username"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
