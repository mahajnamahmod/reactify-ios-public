//
//  ViewController.swift
//  react
//
//  Created by bilaltayh on 9/7/17.
//  Copyright © 2017 bilaltayh. All rights reserved.
//

import UIKit
import SwiftyJSON

var userName = ""

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var invalidMsg: UILabel!
    
    @IBOutlet weak var loginButton: UIButton!
    
    var editEnded: Bool! = false

    var name:String? = ""
    
    
    override func shouldPerformSegue(withIdentifier identifier: String,sender: Any?) -> Bool{
        if(identifier == "createSegue"){
            return true
        }
        return false
    }
    
    @IBAction func loginButton(_ sender: UIButton) {


        if(usernameField.hasText && passwordField.hasText){
            name = usernameField.text
            let password:String? = passwordField.text
            let parameters: [String: Any?] = [
                "username" : name,
                "password" : password
            ]
                
                
            API.shared.login(parameters: parameters, retriesIntervals: [1,1,1,3,3,3,9,9,9], completionHandler: {(success, data) in
                if(success){
                    let json = JSON(data)
                    if(json["statusCode"] == 200){
                        self.usernameField.text = nil
                        self.passwordField.text = nil
                        self.invalidMsg.isHidden = true
                        print("should hide!")
                        userName = parameters["username"] as! String
                        
                        print("sflghsfghodsfjgoisdjfgofdl")
                        print(userName)
                        
                        self.performSegue(withIdentifier: "loginSegue", sender: self)
                    }
                    else{
                        print("should show")
                        self.invalidMsg.isHidden = false
                        print("hidden")
                    }
                }
                })
        }
        else{
            print("should show")
            invalidMsg.isHidden = false
        }
         UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginButton.layer.cornerRadius = 3 // this value vary as per your desire
        self.loginButton.clipsToBounds = true
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        passwordField.isSecureTextEntry = true
        passwordField.placeholder = "Password"
        usernameField.placeholder = "Username"

        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "b1")!)
    }

    func keyboardWillShow(notification: NSNotification) {
        
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            print("HELLO")
//            if self.view.frame.origin.y == 0{
//                self.view.frame = self.view.frame.offsetBy( dx: 0, dy: -200 ); // offset by an amount
//
//            }
//        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            print("HELLO TWO bitch")
//            if self.view.frame.origin.y != 0{
//                self.view.frame = self.view.frame.offsetBy( dx: 0, dy: 200 ); // offset by an amount
//            }
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}





















