

import UIKit
import SwiftyCam
import RecordButton

class ReactViewController: SwiftyCamViewController, SwiftyCamViewControllerDelegate {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    var newView = UIView()
    var instructionsImage = UIImageView()
    @IBOutlet weak var recordingMessage: UILabel!
    var recordButton : RecordButton!
    var progressTimer : Timer!
    var progress : CGFloat! = 0
    var flipCameraButton: UIButton!
    var flashButton: UIButton!
    var captureButton: SwiftyRecordButton!
    
    @IBOutlet weak var frameForButton: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        cameraDelegate = self
        addButtons()
        
        addProgress()
        recordingMessage.isHidden = true
        videoQuality = .high
        swipeToZoom = true
        pinchToZoom = true
        //        doubleTapCameraSwitch = false
        swipeToZoomInverted = false
        defaultCamera = .front
        self.instructions.isHidden = true
        // newView = UIView(frame: self.view.frame)
        //newView.backgroundColor = backColor
        //        self.newView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeInstructions)))
        removeInstructions()
        //self.instructionsImage.frame = self.view.frame
        //self.instructionsImage.image = UIImage(named: "asd")
        //self.newView.addSubview(instructionsImage)
        // self.view.addSubview(newView)
        
    }
    var message =  UIImageView()
    var instructions = UIImageView()
    func removeInstructions (){
        self.instructions.isHidden = false
        UIView.animate(withDuration: 1.0, animations: {
            self.newView.alpha = 0
            
        }, completion: {(boole) in
            self.newView.removeFromSuperview()
        })
    }
    func addProgress(){
        
        message = UIImageView(frame: recordingMessage.frame)
        message.image = UIImage(named: "record")?.resized(to: CGSize(width: 50, height: 50))
        message.contentMode = .scaleAspectFit
        self.view.addSubview(message)
        message.isHidden = true
        
        //instructions.isHidden = false
        // instructions = UIImageView(frame: recordingMessage.frame)
        //instructions.image = UIImage(named: "instr")
        //instructions.contentMode = .scaleAspectFit
        //self.view.addSubview(instructions)
        //instructions.isHidden = false
    }
    
    
    //delegates
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didTake photo: UIImage) {
        // Called when takePhoto() is called or if a SwiftyCamButton initiates a tap gesture
        // Returns a UIImage captured from the current session
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didBeginRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        self.message.isHidden = false
        instructions.isHidden = true
        captureButton.growButton()
        print("recordinggggggggggg")
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishRecordingVideo camera: SwiftyCamViewController.CameraSelection) {
        self.message.isHidden = true
        self.instructions.isHidden = true
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFinishProcessVideoAt url: URL) {
        // Called when stopVideoRecording() is called and the video is finished processing
        // Returns a URL in the temporary directory where video is stored
        captureButton.shrinkButton()
        self.message.isHidden = true
        self.instructions.isHidden = false
        let newVC = ReactVideoViewController(videoURL: url)
        self.present(newVC, animated: true, completion: nil)
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didFocusAtPoint point: CGPoint) {
        // Called when a user initiates a tap gesture on the preview layer
        // Will only be called if tapToFocus = true
        // Returns a CGPoint of the tap location on the preview layer
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didChangeZoomLevel zoom: CGFloat) {
        // Called when a user initiates a pinch gesture on the preview layer
        // Will only be called if pinchToZoomn = true
        // Returns a CGFloat of the current zoom level
    }
    
    func swiftyCam(_ swiftyCam: SwiftyCamViewController, didSwitchCameras camera: SwiftyCamViewController.CameraSelection) {
        // Called when user switches between cameras
        // Returns current camera selection
    }
    
    
    private func addButtons() {
        captureButton = SwiftyRecordButton(frame: CGRect(x: view.frame.midX - 37.5, y: view.frame.height - 100.0, width: 75.0, height: 75.0))
        self.view.addSubview(captureButton)
        captureButton.delegate = self
        
        //  self.view.addSubview(flipCameraButton)
        
        
        //flashButton.setImage(UIImage(named:"recordbutot"), for: UIControlState())
    }
}

