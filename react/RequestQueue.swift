//
//  RequestsQueue.swift
//  SBean
//
//  Created by Eyal Silberman on 15/05/2017.
//  Copyright © 2017 200apps. All rights reserved.
//

import Foundation
class RequestsQueue: NSObject {
    
    static let sharedInstance = RequestsQueue()
    
    let operationQueue = OperationQueue()
    
    // semaphore with count equal to zero is useful for synchronizing completion of work, in our case the renewal of auth token
    let sema = DispatchSemaphore(value: 0);
    
    required override init() {
        super.init()
        
        operationQueue.maxConcurrentOperationCount = 1
    }
    func addOperation(operation: Operation, queuePriority: Operation.QueuePriority) {
        //        operationQueue.isSuspended = true
        operation.queuePriority = queuePriority
        operationQueue.addOperation(operation)
        //        operationQueue.isSuspended = false
    }
    
}
