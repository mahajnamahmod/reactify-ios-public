//
//  PostRequest.swift
//  SBean
//
//  Created by Bilal Taya on 15/05/2017.
//

import Foundation
import Alamofire
class PostRequest: Operation{
    
    var host: String?
    var path: String?
    var method: HTTPMethod = .get
    var parameters: Parameters = [:]
    var headers: HTTPHeaders = [:]
    var retryNum: Int = 0
    var retriesIntervals: [TimeInterval] = []
    var completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?
    
    @discardableResult required init(host: String?, path: String?, method: HTTPMethod, parameters: Parameters, headers: HTTPHeaders, retriesIntervals: [TimeInterval], priority: Operation.QueuePriority, completionHandler: ((_ success: Bool, _ data: [String: AnyObject]?) -> Void)?) {
        super.init()
        
        self.host = host
        self.path = path
        self.method = method
        self.parameters = parameters
        self.headers = headers
        self.retriesIntervals = retriesIntervals
        self.completionHandler = completionHandler
        
        RequestsQueue.sharedInstance.addOperation(operation: self, queuePriority: .high)
    }
    
    override func main() {
        print("REQUEST NOT PREFORMED!")

        performRequest()
        
//        // wait until the response block will send a signal
//        let _ = RequestsQueue.sharedInstance.sema.wait(timeout: DispatchTime.distantFuture)
    }
    
    func performRequest() {
        //        parameters["retry_count"] = retryNum
        if let _ = host, let path = path {
            let request = Alamofire.request(DEV_DOMAIN + path, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            
            request.responseJSON { response in
                
                
                // check if responseJSON already has an error
                // e.g., no network connection
                if let json = response.result.value {
                    
                    print("--------")
                    print(json)
                    print("--------")
                }
                guard response.result.error == nil else {
                    print(response.result.error?.localizedDescription ?? "Response Error")
                    self.completionHandler?(response.result.isSuccess, nil)
                    self.retryRequest()
                    return
                }
                
                // make sure we got JSON and it's a dictionary
                guard let json = response.result.value as? [String: AnyObject] else {
                    print("didn't get dictionary object as JSON from API")
                    self.completionHandler?(response.result.isSuccess, nil)
                    self.retryRequest()
                    return
                }
                
                // make sure status code is 200
                guard response.response?.statusCode == 200 else {
                    // handle status code
                    
                    self.completionHandler?(response.result.isSuccess, json)
                    return
                }
                
                self.completionHandler?(response.result.isSuccess, json)
//                RequestsQueue.sharedInstance.sema.signal()
                
            }
            print("JSON PREFORMED!")
        }
    }
    
    func retryRequest() {
        if retriesIntervals.count > retryNum {
            Timer.scheduledTimer(timeInterval: retriesIntervals[self.retryNum], target: self, selector: #selector(PostRequest.performRequest), userInfo: nil, repeats: false)
            retryNum += 1
        }
    }
    
    
}
