//
//  popupAddFriendViewController.swift
//  react
//
//  Created by bilaltayh on 9/10/17.
//  Copyright © 2017 bilaltayh. All rights reserved.
//

import UIKit
import SwiftyJSON

class popupAddFriendViewController: UIViewController {
    
    
    
    var delegate:friendDelegate?
    @IBOutlet weak var friendName: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    var passedName =  String()
    var passedSearch = UITableView()
    var passedImg = UIImage()
    var tableView = UITableView()
    @IBAction func cancel(_ sender: UIButton) {
        self.removeAnimate()
        passedSearch.isScrollEnabled = true
        tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        //        self.navigationController?.navigationBar.layer.zPosition = 0;
        
    }
    
    
    @IBAction func addFriend(_ sender: UIButton) {
        
        let parameters: [String: Any?] = [
            "user1" : userName,
            "user2" : passedName
        ]
        print(userName)
        print(passedName)
        
        
        API.shared.addFriend(parameters: parameters, retriesIntervals: [1,1,1,3,3,3,9,9,9], completionHandler: {(success, data) in
            if(success){
                let json = JSON(data)
                if(json["create"] == "true"){
                    print("added")
                    self.delegate?.reload()
                    self.cancel(sender)
                    
                }
                else{
                    print("should show")
                    
                }
            }
        })
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passedSearch.isScrollEnabled = false
        passedSearch.contentOffset = CGPoint(x:0,y:0)
        self.profileImg.layer.cornerRadius = 5.0
        self.profileImg.layer.masksToBounds = true
        self.profileImg.contentMode = .scaleAspectFill
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
        print(passedName)
        self.friendName.text = passedName
        self.profileImg.image = passedImg
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.navigationController?.setNavigationBarHidden(true
            , animated: true)
        //        tableView.contentOffset = CGPoint(x: 0,y:0)
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
