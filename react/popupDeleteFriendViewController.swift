//
//  popupAddFriendViewController.swift
//  react
//
//  Created by bilaltayh on 9/10/17.
//  Copyright © 2017 bilaltayh. All rights reserved.
//

import UIKit
import SwiftyJSON


class popupDeleteFriendViewController: UIViewController {
    
    
    var delegate:friendDelegate?
    @IBOutlet weak var friendName: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    var tableView = UITableView()
    var passedName =  String()
    var passedSearch = UITableView()
    var passedImg = UIImage()
    
    @IBAction func cancel(_ sender: UIButton) {
        // self.view.removeFromSuperview()
        self.removeAnimate()
        passedSearch.isScrollEnabled = true
        tableView.contentOffset = CGPoint(x: 0, y: -65)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    
    
    @IBAction func DeleteFriend(_ sender: UIButton) {
        
                print("sfjksdljfgasjgdfasgdfljkaslkdsaldalksjd")
        
                let parameters: [String: Any?] = [
                    "username" : userName,
                    "friend" : passedName
                ]
                print(userName)
                print(passedName)
        
        
                API.shared.deleteFriends(parameters: parameters, retriesIntervals: [1,1,1,3,3,3,9,9,9], completionHandler: {(success, data) in
                    if(success){
                        print("deleted")
                        self.delegate?.reload()
                        self.cancel(sender)
  
                    }
                })
        
        
    }
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passedSearch.isScrollEnabled = false
        passedSearch.contentOffset = CGPoint(x:0,y:0)
        self.profileImg.layer.cornerRadius = 5.0
        self.profileImg.layer.masksToBounds = true
        self.profileImg.image = passedImg
        self.profileImg.contentMode = .scaleAspectFill

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
        self.friendName.text = passedName
        tableView.contentOffset = CGPoint(x: 0,y:0)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
